    angular.module('HelloWorldApp', [])
       .controller('HelloWorldController', function($interval,$http,$scope) {
           $scope.text = "";

    $interval(callAtInterval, 2000);

	function callAtInterval() {
	    console.log("Interval occurred");
		   $http({
		      method: 'GET',
		      url: '/api/temperaturi'
		   }).then(function (data){
		   		$scope.text=data['data'];
		   },function (error){
		   		console.log("endpoint not available");
		   });

	}

    });