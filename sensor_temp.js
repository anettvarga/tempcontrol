//MQTT connection
var mqtt = require('mqtt');
var client  = mqtt.connect('mqtt://test.mosquitto.org');

//mailing settings
var nodemailer = require('nodemailer'); 

//express
var fs = require('fs');
var express = require('express');
var path= require('path');
var app = express();
var cors = require('cors');
app.use(cors());
app.use('/static', express.static(path.join(__dirname, 'public')))

app.listen(9090, function () {
    console.log('Dev app listening on port 9090!');
});

var temperaturi = new Map();

app.route('/api/temperaturi').get((req, res) => {
	var temp_object = {};

	temperaturi.forEach((value, key) => {
	    var keys = key.split('.'),
	        last = keys.pop();
	    keys.reduce((r, a) => r[a] = r[a] || {}, temp_object)[last] = value;
	});

	console.log(temp_object);	
	res.send(temp_object);
});

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: 
  {
  	user: 'test.bot1nski@gmail.com',
    pass: '12345678Abc'
  }
});

var diconnectionMailhOptions = {
  from: 'youremail@gmail.com',
  to: 'dinu.taleanu@gmail.com',
  subject: 'It\'s dead!',
  text: 'The ESP8266 has stopped working!'
};1

var tempTooHighMailOptions = {
  from: 'youremail@gmail.com',
  to: 'dinu.taleanu@gmail.com',
  subject: 'Too hot!',
  text: 'The temperature of the room has risen so much that Hell seems like a nice Holiday resort!'
};

var tempTooLowMailOptions = {
  from: 'youremail@gmail.com',
  to: 'dinu.taleanu@gmail.com',
  subject: 'Too cold',
  text: 'The temperature of the room dropped so much that penguins started spawning inside of it!'
};

//today's date
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) 
    dd = '0'+dd

if(mm<10) 
    mm = '0'+mm

today = mm + '/' + dd + '/' + yyyy;

//actual program
client.on('connect', function () 
{
  client.subscribe('ro/tempcontrol');
})

var elapsedTime = 0;
var infoArray = {};
var lastMailSent = "";

client.on('message', function(topic, message)
{
	if(topic == 'ro/tempcontrol')
	{
		elapsedTime = 0;
		infoArray = JSON.parse(message);

		if(infoArray['deviceid'] == 'Node-2066268' || infoArray['deviceid'] == 'Node-2523011')
			{
				if (infoArray['deviceid']  == 'Node-2066268') infoArray['deviceid'] = "Birou"; else infoArray['deviceid'] = "Data center";
	            temperaturi.set(infoArray['deviceid'],infoArray['value']);

				console.log(topic + ": " + message);
				if(lastMailSent !== today)
			{	
				if(infoArray['value'] >= 35)
				{
					transporter.sendMail(tempTooHighMailOptions, function(error, info)
					{
						if(error)
							console.log("Message sending failed. Error : " + info);
					});

					lastMailSent = today;
				}

				if(infoArray['value'] < 10)
				{
					transporter.sendMail(tempTooLowMailOptions, function(error, info)
					{
						if(error)
							console.log("Message sending failed. Error : " + info);
					});

					lastMailSent = today;
				}
			}
		}
	}
})

setInterval(function()
{
	elapsedTime++;

	if(elapsedTime == 600) //10 min
	{
		console.log("The ESP8266 has dieded! R.I.P. . .")
		transporter.sendMail(diconnectionMailhOptions, function(error, info)
		{
			if(error)
				console.log("Message sending failed. Error : " + info);

			client.end();
		});
	}

}, 1000); //once per sec
